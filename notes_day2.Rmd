---
title: "Notes Day 2"
author: "Andreas Gravgaard Andersen"
date: "01/12/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(rethinking)
```


# Day 2 -- Tuesday, 1.12:

8.30-10.00 -- 1st block (90 min): Linear models and linear regression intro (SR 4.4)

Lecturer: Søren Besenbacher

Teaching assistants: Simon Grund, Gustav Poulsgaard & Asta Rasmussen


Topics:

 1. Linear model specification (25 min)
    1. Inference, predictive intervals, etc
 2. Exercises (25 min) 4M3 + 4H2 (+ M4-5)
     * 4M3:
       y ~ normal(mu, sigma)
       mu = a + b x
       a ~ normal(0, 10)
       b ~ uniform(0, 1)
       sigma ~ exp(1)
     * 4M4:
       y <- heigth, x <- year
       y ~ normal(mu, sigma)
       mu = a + b x
       a ~ normal(178, 0)
       b ~ uniform(0, 10)
       sigma ~ exp(1)
     * 4M5: That's why b can't be negative
     * 4H2:
```{r}
data(Howell1)
d <- Howell1
d2 <- d[d$age < 18 , ]
nrow(d2)

flist <- alist(height ~ dnorm(mu , sigma) ,
               mu ~ dnorm(168 , 20) ,
               sigma ~ dunif(0 , 50))

m4h2 <- quap(flist , data = d2)
precis(m4h2)

plot(d2$height ~ d2$weight)

post <- extract.samples(m4h2)
a_map <- mean(post$mu)
b_map <- mean(post$sigma)
curve(a_map + b_map * x , add = TRUE)

d2$weight_s <- (d2$weight - mean(d2$weight)) / sd(d2$weight)
d2$weight_s2 <- d2$weight_s^2
flist <- alist(height ~ dnorm(mu , sigma) ,
               mu <- a + b1 * weight_s + b2 * weight_s2,
               a ~ dnorm(178, 20),
               b1 ~ dlnorm(0, 1),
               b2 ~ dnorm(0, 1),
               sigma ~ dunif(0 , 50))
m4h2 <- quap(flist , data = d2)
weight.seq <- seq(from=2.2, to=2, lenght.out=30)
pred_dat <- list(weight_s=weight.seq, weight_s2=weight.seq^2)

mu <- link(m4h2, data=pred_dat)
```
       
 
 3. Polynomial regression and splines (SR 4.5) (25 min)
 4. Exercises (15 min) (4M8 + 4H5)
```{r}
data(cherry_blossoms)
d <- cherry_blossoms
precis(d)

d2 <-
  d[complete.cases(d$doy) , ] # complete cases on doy
num_knots <- 3
knot_list <-
  quantile(d2$year , probs = seq(0, 1, length.out = num_knots))

## R code 4.74
library(splines)
B <- bs(d2$year,
        knots = knot_list[-c(1, num_knots)] ,
        degree = 3 ,
        intercept = TRUE)

## R code 4.75
plot(
  NULL ,
  xlim = range(d2$year) ,
  ylim = c(0, 1) ,
  xlab = "year" ,
  ylab = "basis"
)
for (i in 1:ncol(B))
  lines(d2$year , B[, i])

## R code 4.76
m4.7 <- quap(
  alist(
    D ~ dnorm(mu , sigma) ,
    mu <- a + B %*% w ,
    a ~ dnorm(100, 10),
    w ~ dnorm(0, 10),
    sigma ~ dexp(1)
  ),
  data = list(D = d2$doy , B = B) ,
  start = list(w = rep(0 , ncol(B)))
)

post <- extract.samples(m4.7)
w <- apply(post$w , 2 , mean)
plot(
  NULL ,
  xlim = range(d2$year) ,
  ylim = c(-6, 6) ,
  xlab = "year" ,
  ylab = "basis * weight"
)
for (i in 1:ncol(B))
  lines(d2$year , w[i] * B[, i])

## R code 4.78
mu <- link(m4.7)
mu_PI <- apply(mu, 2, PI, 0.97)
plot(d2$year , d2$doy , col = col.alpha(rangi2, 0.3) , pch = 16)
shade(mu_PI , d2$year , col = col.alpha("black", 0.5))

```
 
```{r}
plot(d2$doy ~ d2$temp)
```

10.00-10.15: break

10.15-11.45 -- 2nd block (90 min): Multivariate linear regression and confounders
Lecturer: Søren Besenbacher

Teaching assistants: Simon Grund, Gustav Poulsgaard, and Asta Rasmussen

Topics:

 1. Multivariate regression (SR 5.1): Model specification, Standard compact notation (design matrix) (15 min)
 2. Exercises (15 min) 5E3 + 5M4 + 5H1
      Data for 5M4:
```{r}
data("WaffleDivorce")
d <- WaffleDivorce
precis(d)

d$D <- standardize(d$Divorce)
d$M <- standardize(d$Marriage)
d$A <- standardize(d$MedianAgeMarriage)
d$pct_LDS <- c(0.75, 4.53, 6.18, 1, 2.01, 2.82, 0.43, 0.55, 0.38, 0.75, 0.82, 5.18, 26.35, 0.44, 0.66, 0.87, 1.25, 0.77, 0.64, 0.81, 0.72, 0.39, 0.44, 0.58, 0.72, 1.14, 4.78, 1.29, 0.61, 0.37, 3.34, 0.41, 0.82, 1.48, 0.52, 1.2, 3.85, 0.4, 0.37, 0.83, 1.27, 0.75, 1.21, 67.97, 0.74, 1.13, 3.99, 0.92, 0.44, 11.5 )
d$L <- standardize(d$pct_LDS)

m5.1 <- quap(alist(
  D ~ dnorm(mu , sigma) ,
  mu <- a + bA * A + bM * M + bL * L,
  a ~ dnorm(0 , 0.2),
  bA ~ dnorm(0 , 0.5),
  bM ~ dnorm(0 , 0.5),
  bL ~ dnorm(0 , 0.5),
  sigma ~ dexp(1)
),
data = d)

A_seq <- seq(from = -3 ,
             to = 3.2 ,
             length.out = 30)
M_seq <- seq(from = -3 ,
             to = 3.2 ,
             length.out = 30)
L_seq <- seq(from = 0 ,
             to = 1 ,
             length.out = 30)
mu <- link(m5.1 , data = list(A = A_seq, M = M_seq, L = L_seq))
mu.mean <- apply(mu , 2, mean)
mu.PI <- apply(mu , 2 , PI)

# plot it all
plot(D ~ A , data = d , col = rangi2)
lines(A_seq , mu.mean , lwd = 2)
shade(mu.PI , A_seq)
```

 3. Categorical variables (SR5.3) (15 min)
```{r}
## R code 5.45
data(Howell1)
d <- Howell1
str(d)

## R code 5.46
mu_female <- rnorm(1e4, 178, 20)
mu_male <- rnorm(1e4, 178, 20) + rnorm(1e4, 0, 10)
precis(data.frame(mu_female , mu_male))

## R code 5.47
d$sex <- ifelse(d$male == 1 , 2 , 1)
str(d$sex)

## R code 5.48
m5.8 <- quap(alist(
  height ~ dnorm(mu , sigma) ,
  mu <- a[sex] ,
  a[sex] ~ dnorm(178 , 20) ,
  sigma ~ dunif(0 , 50)
) , data = d)
precis(m5.8 , depth = 2)
```
 4. Confounders and DAGs (SR 5.2 + SR 6.1 + SRS 6.4) (25 min)
```{r}
library(dagitty)
dag_6.2 <- dagitty("dag {
    A -> D
    A -> M -> D
    A <- S -> M
    S -> W -> D
    S -> A -> D
}")

drawdag(dag_6.2)

adjustmentSets(dag_6.2 , exposure = "W" , outcome = "D")

## R code 6.31
impliedConditionalIndependencies(dag_6.2)

```
 5. Exercises (20 min) 5E4 + 6M1 + 6H3-5
```{r}
dag_6.2 <- dagitty("dag {
    area -> avgfood -> weight
    avgfood -> groupsize -> weight
}")

drawdag(dag_6.2)

adjustmentSets(dag_6.2 , exposure = "area" , outcome = "weight")

## R code 6.31
impliedConditionalIndependencies(dag_6.2)

dev.off()

data("foxes")
d <- foxes
d$A <- standardize(d$area)
d$W <- standardize(d$weight)

m6.2 <- quap(alist(
  W ~ dnorm(mu , sigma) ,
  mu <- a + bA * A ,
  a ~ dnorm(0 , 0.2),
  bA ~ dnorm(0 , 0.5),
  sigma ~ dexp(1)
) , data = d)
precis(m6.2 , depth = 2)

A_seq <- seq(from = -3 ,
             to = 3 ,
             length.out = 30)
mu <- link(m6.2 , data = list(A = A_seq))
mu.mean <- apply(mu , 2, mean)
mu.PI <- apply(mu , 2 , PI)

# plot it all
plot(W ~ A , data = d)
lines(A_seq , mu.mean , lwd = 2)
shade(mu.PI , A_seq)

```

```{r}
adjustmentSets(dag_6.2 , exposure = "avgfood" , outcome = "weight")
impliedConditionalIndependencies(dag_6.2)
# We can assume area is independent from weight based on the results above

d$A <- standardize(d$avgfood)

m6.2 <- quap(alist(
  W ~ dnorm(mu , sigma) ,
  mu <- a + bA * A ,
  a ~ dnorm(0 , 0.2),
  bA ~ dnorm(0 , 0.5),
  sigma ~ dexp(1)
) , data = d)
precis(m6.2 , depth = 2)

A_seq <- seq(from = -3 ,
             to = 3 ,
             length.out = 30)
mu <- link(m6.2 , data = list(A = A_seq))
mu.mean <- apply(mu , 2, mean)
mu.PI <- apply(mu , 2 , PI)

# plot it all
plot(W ~ A , data = d)
lines(A_seq , mu.mean , lwd = 2)
shade(mu.PI , A_seq)

```

```{r}
adjustmentSets(dag_6.2 , exposure = "groupsize" , outcome = "weight")
impliedConditionalIndependencies(dag_6.2)
# We can assume area and avgfood is independent from weight based on the results above

d$A <- standardize(d$groupsize)

m6.2 <- quap(alist(
  W ~ dnorm(mu , sigma) ,
  mu <- a + bA * A ,
  a ~ dnorm(0 , 0.2),
  bA ~ dnorm(0 , 0.5),
  sigma ~ dexp(1)
) , data = d)
precis(m6.2 , depth = 2)

A_seq <- seq(from = -3 ,
             to = 3 ,
             length.out = 30)
mu <- link(m6.2 , data = list(A = A_seq))
mu.mean <- apply(mu , 2, mean)
mu.PI <- apply(mu , 2 , PI)

# plot it all
plot(W ~ A , data = d)
lines(A_seq , mu.mean , lwd = 2)
shade(mu.PI , A_seq)

```

11.45-12.30: Lunch

12:30-13.30 -- Guest lecture: Prof Asger Hobolth: Bayesian ranking

13.30-13.40: Break

13.40-15.15 -- 3rd block (95 min): Model selection I

Lecturer: Simon Drue

Teaching assistant: Mikkel Hovden & Jørn Bethune

Topics:

 1. Introduction to model selection (15 min)
 2. Number of free parameters and overfitting (SR 7.1) (15 min)
 3. Exercises (10 min)
    1. R exercises with simulated data from polynomial models
        1. Ex. A: Quantitative fit of models
        2. Ex. B: Visual fit of models
        3. Ex. C: Sensitivity to samples (leave one sample out)
            * See "Model Selection[...].R"
 4. Model performance measures (SR 7.2) (25 min)
     1. Motivation
     2. Information theory: Entropy, KL-divergence, and deviance
 5. In-sample and out of sample performance (SR 7.2) (5 min)
 6. Exercises (15 min)
     1. R exercises with simulated data from polynomial models
         1. Ex. D: Deviance of poly models
         2. Ex. E: In and out of sample deviance
            * See "Model Selection[...].R"
     2. From SR:
         1. Exercises about coin flip/die toss from book (7E2-7E4)
         2. Bird island exercise (7H3) [Optional]

15.15-15.30: Break

15.30-16.30 -- 4th block (60 min): Model selection II

Lecturer: Simon Drue

Teaching assistant: Mikkel Hovden & Jørn Bethune

Topics:

 7. Regularization (SR 7.3) (10 min)
 8. Predicting predictive accuracy (SR 7.4)
   1. Cross-validation (10 min)
   2. Information criteria (15 min)
 9. Exercises (35 min)
   1. R exercises with simulated data from polynomial models
     1. Ex. F: The effect of regularization
     2. Ex. G: PSIS and WAIC for models
     3. Ex. H: Effective number of parameters
            * See "Model Selection[...].R"
   2. From SR:
     1. Why should you compare models on the same data? (7M3)
     2. Revisit foxes dataset (7H5) [Optional]
   3. Finish above exercises

