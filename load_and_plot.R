library(readr)
library(dplyr)
d <- read_tsv("mutation_count_by_type.tsv")

#Plot
plot(all_mutations~ Father_age, data=d)
plot(all_mutations~ Mother_age, data=d)

#Remove outliers
d <- d %>% filter(Father_age>40 | all_mutations<140) 