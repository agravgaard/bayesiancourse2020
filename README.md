## Bayesian course notes and exercises

Course material is available at [github.com/rmcelreath/rethinking](https://github.com/rmcelreath/rethinking), also under GPLv3

R notebooks in HTML: 
- [Day 1](https://agravgaard.gitlab.io/bayesiancourse2020/notes.html)
- [Day 2](https://agravgaard.gitlab.io/bayesiancourse2020/notes_day2.html)
- [Day 3](https://agravgaard.gitlab.io/bayesiancourse2020/notes_day3.html)
- Day 4 was the hackathon
- Day 5 was presentation of work during hackathon
