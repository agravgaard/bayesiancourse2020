---
title: "Day4_Hackathon"
author: "Andreas Gravgaard Andersen"
date: "03/12/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(rethinking)
rstan_options(auto_write = TRUE)
library(tidyverse)
```


# Day 4

```{r}
d <- read_tsv("mutation_count_by_type.tsv")

#Remove outliers
d <- d %>% filter(Father_age>40 | all_mutations<140) 
d_lmf <- d %>% pivot_longer(c(Father_age, Mother_age), names_to = "Parent", values_to = "age")

#Plot
png("Raw_data.png")
d_lmf %>% ggplot(aes(x = age, y = all_mutations, color = Parent)) +
  geom_point()
dev.off()
```

```{r}
v_mutations <- c("A2C", "A2G", "A2T", "C2G", "C2A", "C2T", "indel")
d_lmf_lmut <- d %>% pivot_longer(v_mutations, names_to = "Mutation", values_to = "N") %>%
  filter(Father_age < max(d$Mother_age))

png("by_type_vs_parents_age.png")

d_lmf_lmut %>% ggplot(aes(x = (Mother_age + Father_age) / 2 , y = N)) +
  geom_point(aes(color = Father_age - Mother_age, alpha=.5)) +
  geom_smooth(method = "lm") +
  scale_color_viridis_c() +
  facet_wrap( ~ Mutation)

dev.off()

```

```{r}
d_lmf_lmut %>% ggplot(aes(x=N)) +
  geom_density() +
  facet_wrap( ~ Mutation)
```

```{r}
m_qpois <- list()
d_imut <- list()
i <- 1
for (mut in v_mutations) {
  d_imut[[i]] <- list(
    FA = d$Father_age, 
    MA = d$Mother_age,
    N = d[[mut]]
  )
  m_qpois[[i]] <- quap(
    alist(
      N ~ dpois(lambda),
      log(lambda) <- a + bF * FA + bM * MA,
      c(a, bF, bM) ~ dnorm(0, 1.5)
    ),
    data = d_imut[[i]]
  )
  i = i + 1
}

waic_m_qpois <- do.call(compare, m_qpois)
rownames(waic_m_qpois) <- v_mutations
waic_m_qpois

i <- 1
pr_qpois <- list()
for (mut in v_mutations) {
  pr_qpois[[i]] <- data.frame(precis(m_qpois[[i]]))
  pr_qpois[[i]]$Mutation <- mut
  pr_qpois[[i]] <- pr_qpois[[i]] %>% rownames_to_column(var = "Variable")
  i = i + 1 
}

df_pr_qpois <- do.call(rbind, pr_qpois) 

df_pr_qpois %>% ggplot(aes(x = Variable, y = mean)) +
  geom_point() +
  geom_errorbar(aes(ymin = X5.5., ymax = X94.5.)) +
  facet_wrap(~ Mutation)
```

```{r}
par(mfrow = c(3, 3))
i <- 1 
nsim <- 100
d_mut <- list(
  FA = seq(min(d$Father_age), max(d$Father_age), nsim), 
  MA = seq(min(d$Mother_age), max(d$Mother_age), nsim) 
)

for (mut in v_mutations) {
  s <- sim(m_qpois[[i]], d_mut)
  # hS <- hist(s, plot=FALSE)
  # hD <- hist(d_imut[[i]]$N, plot=FALSE)
  plot(density(s), col = "red", main = mut)
  lines(density(d_imut[[i]]$N), col = "black")
  i = i + 1
}


```


```{r}
d_slim <- list(
  FA = d_lmf_lmut$Father_age, 
  MA = d_lmf_lmut$Mother_age,
  N = d_lmf_lmut$N,
  Mut = as.factor(d_lmf_lmut$Mutation)
)
m_qpois_all <- quap(
  alist(
    N ~ dpois(lambda),
    log(lambda) <- a[Mut] + bF[Mut] * FA + bM[Mut] * MA,
    a[Mut] ~ dnorm(0, 1.5),
    bF[Mut] ~ dnorm(0, 1.5),
    bM[Mut] ~ dnorm(0, 1.5)
  ),
  data = d_slim
)
```

```{r}
d_slim_test <- list(
  FA = d_lmf_lmut$Father_age, 
  MA = d_lmf_lmut$Mother_age,
  Mut = as.factor(d_lmf_lmut$Mutation)
)
d_test <- data.frame(d_slim_test) %>% dplyr::sample_n(1000)

s <- sim(m_qpois_all, d_test, 500)
plot(density(s, bw = .5), col = "red", main = mut)
lines(density(d_slim$N, bw = .5), col = "black")
```


```{r}

m_ulam <- read_rds("mult_lev_reg_new.rds")
precis(m_ulam)

d_test_u <- list(
  Father_age = d_test$FA,
  Mother_age = d_test$MA,
  type = d_test$Mut
)
s_u <- sim(m_ulam, d_test_u)
plot(density(s), col = "red", main = "Compare")
lines(density(s_u, bw = .5), col = "blue")
lines(density(d_slim$N, bw = .5), col = "black")
```

```{r}
d_test <- list(
  FA = d_lmf_lmut$Father_age, 
  MA = d_lmf_lmut$Mother_age,
  Mut = as.factor(d_lmf_lmut$Mutation)
)
my_bw <- 0.7
pdf("dens_quap_vs_ulam.png")
par(mfrow = c(3, 3))
for (mut in v_mutations) {
  d_test <- data.frame(d_slim_test) %>%
    filter(Mut == mut) %>%
    dplyr::sample_n(1000)
  
  d_test_u <- list(
    Father_age = d_test$FA,
    Mother_age = d_test$MA,
    type = d_test$Mut
  )
  s_u <- sim(m_ulam, d_test_u, 500)
  s <- sim(m_qpois_all, d_test, 500)
  raw_data <- data.frame(d_slim) %>%
    filter(Mut == which(v_mutations == mut)) 
  
  plot(density(s, bw = my_bw), col = "red", main = paste("Compare", mut))
  xlab("# of mutations")
  lines(density(s_u, bw = my_bw), col = "blue")
  lines(density(raw_data$N, bw = my_bw), col = "black")
}

d_test <- data.frame(d_slim_test) %>%
  dplyr::sample_n(1000)

d_test_u <- list(
  Father_age = d_test$FA,
  Mother_age = d_test$MA,
  type = d_test$Mut
)
s_u <- sim(m_ulam, d_test_u, 500)
s <- sim(m_qpois_all, d_test, 500)
raw_data <- data.frame(d_slim)

plot(density(s, bw = my_bw), col = "red", main = paste("Compare", "All"))
lines(density(s_u, bw = my_bw), col = "blue")
lines(density(raw_data$N, bw = my_bw), col = "black")

dev.off()
```


```{r}
library(bayesplot)

```